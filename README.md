Prerequisites
-----

Have Docker installed and running on your machine.

See the [Docker website](http://www.docker.io/gettingstarted/#h_installation) for installation instructions.

Build
-----

Steps to build the project with Docker:

1. Clone this repo

        git clone git@gitlab.com:uzairriaz/news-aggregator-frontend.git

2. **Important:** Change to the repo directory and copy `.env.example` and rename it to `.env`

        cd news-aggregator-frontend
        cp .env.example .env


2. Build and run the project (This will take a few minutes.)

        docker compose up

3. Once everything has started up, you should be able to access the app via [http://localhost:3000/](http://localhost:3000/) on your host machine.

        http://localhost:3000/
