import {useCallback, useMemo} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import Auth from '../../api/Auth';
import Form from '../common/Form'
import { Col, Row, Container, Card } from 'react-bootstrap';
import { faEnvelope, faLock, faSignature } from '@fortawesome/free-solid-svg-icons';

export const Register = () => {
    const navigate = useNavigate();

    const register = useCallback(() => navigate('/login', {replace: true}), [navigate])

    const fields = useMemo(() => [
        {
            name: 'name',
            placeholder: 'Name',
            type: 'text',
            icon: faSignature,
        },
        {
            name: 'email',
            placeholder: 'Email',
            type: 'email',
            icon: faEnvelope,
        },
        {
            name: 'password',
            placeholder: 'Password',
            type: 'password',
            icon: faLock,
        },
        {
            name: 'password_confirmation',
            placeholder: 'Confirm Password',
            type: 'password',
            icon: faLock,
        }
    ], []);

    if (localStorage.getItem('accessToken')) {
        return <Navigate to="/" replace />
    }

    return (
        <div className="registration-form">
            <Container>
                <Row className="vh-100 d-flex justify-content-center align-items-center">
                    <Col md={8} lg={6} xs={12}>
                        <div className="border border-2 border-primary"></div>
                        <Card className="shadow px-4 dark">
                            <Card.Body>
                                <div className="mb-3 mt-md-4">
                                    <div className="mb-3">
                                        <Form
                                            fields={fields}
                                            action={Auth.register}
                                            successCallback={register}
                                            submitText="Register"
                                            submittingText="Registering"
                                        />

                                        <div className="mt-3">
                                            <p className="mb-0  text-center">
                                                Already have an account?{" "}
                                                <a href="/login" className="text-primary fw-bold">
                                                    Sign In
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Register;