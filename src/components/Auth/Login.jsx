import {useCallback, useMemo} from 'react';
import {Navigate} from 'react-router-dom';
import Auth from '../../api/Auth';
import Form from '../common/Form'
import {Col, Row, Container, Card} from 'react-bootstrap';
import { faEnvelope, faLock } from '@fortawesome/free-solid-svg-icons';

export const Login = () => {

    const login = useCallback(({data}) => {
        localStorage.setItem('accessToken', data.accessToken);
        window.location.href = '/';
    }, []);

    const fields = useMemo(() => [
        {
            name: 'email',
            placeholder: 'Email',
            type: 'email',
            icon: faEnvelope,
        },
        {
            name: 'password',
            placeholder: 'Password',
            type: 'password',
            icon: faLock,
        },
    ], []);

    if (localStorage.getItem('accessToken')) {
        return <Navigate to="/" replace />
    }

    return (
        <div className="login-form">
            <Container>
                <Row className="vh-100 d-flex justify-content-center align-items-center">
                    <Col md={8} lg={6} xs={12}>
                        <div className="border border-2 border-primary"></div>
                        <Card className="shadow px-4 dark">
                            <Card.Body>
                                <div className="mb-3 mt-md-4">
                                    <div className="mb-3">
                                        <Form
                                            fields={fields}
                                            action={Auth.login}
                                            successCallback={login}
                                            submitText="Login"
                                            submittingText="Logging in"
                                        />

                                        <div className="mt-3">
                                            <p className="mb-0  text-center">
                                                Not registered yet?{" "}
                                                <a href="/register" className="text-primary fw-bold">
                                                    Create an account
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Login;