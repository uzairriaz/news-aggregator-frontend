import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import Articles from '../../api/Articles';

const ArticleDetail = () => {
    const {id} = useParams();
    const [article, setArticle] = useState({});
    const {title, description, content, author, image_url, published_at} = article;

    useEffect(() => {
        Articles.get(id).then(({data}) => setArticle(data))
    }, [id, setArticle]);

    return <section className="dark pt-2">
        <div className="container py-4">
            <article className="postcard dark">
                <a className="postcard__img_link" href={`/detail/${id}`}>
                    <img className="postcard__img" src={image_url ? image_url : 'https://artsmidnorthcoast.com/wp-content/uploads/2014/05/no-image-available-icon-6.png'} alt="article"/>
                </a>
                <div className="postcard__text">
                    <h1 className="postcard__title"><a href={`/detail/${id}`}>{title}</a></h1>
                    <div className="postcard__subtitle small">
                        <time dateTime="2020-05-25 12:00:00">
                            <i className="fas fa-calendar-alt mr-2"></i>{new Date(published_at).toDateString()}
                        </time>
                    </div>
                    <div className="postcard__bar"></div>
                    <div className="postcard__preview-txt" dangerouslySetInnerHTML={{__html: description}} />
                    {author && <ul className="postcard__tagbox">
                        <li className="tag__item play">
                            <i className="fas fa-play mr-2"></i>{author}
                        </li>
                    </ul>}
                </div>
            </article>
            {content && <article className="shadow-lg p-3 mb-5 bg-dark rounded">
                <div className="text-justify" dangerouslySetInnerHTML={{__html: content}} />
            </article>}
        </div>
    </section>
};

export default ArticleDetail;