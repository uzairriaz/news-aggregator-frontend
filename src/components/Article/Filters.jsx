import {useEffect, useState} from 'react';
import {Col, Form, Row} from 'react-bootstrap';
import Categories from '../../api/Categories';
import Sources from '../../api/Sources';
import Select from 'react-select';
import DatePicker from 'rsuite/DatePicker';

const ArticleFilters = ({ onFilterChange }) => {
    const [categories, setCategories] = useState([]);
    const [sources, setSources] = useState([]);

    useEffect(() => {
        Categories.list({preferred: true})
            .then(({data}) => setCategories(data))
            .catch((error) => console.error(error));

        Sources.list({preferred: true})
            .then(({data}) => setSources(data))
            .catch((error) => console.error(error));
    }, []);

    return (
        <Form>
            <Row>
                <Col sm={6} md={3}>
                    <Form.Control className="m-1" type="text" placeholder="Search..." onChange={e => onFilterChange('q', e.target.value)} />
                </Col>
                <Col sm={6} md={3}>
                    <DatePicker
                        oneTap
                        onChange={date => onFilterChange('date', date ? date.toISOString().split("T")[0] : undefined)}
                        className="w-100 m-1"
                        placeholder="Date"
                        format="yyyy-MM-dd"
                    />
                </Col>
                <Col sm={6} md={3}>
                    <Select
                        isClearable
                        placeholder="Select category"
                        options={categories.map(category => ({value: category.id, label: category.name}))}
                        onChange={opt => onFilterChange('category_id', opt ? opt.value : undefined)}
                        className="m-1"
                        classNamePrefix="react-select"
                    />
                </Col>
                <Col sm={6} md={3}>
                    <Select
                        isClearable
                        placeholder="Select source"
                        options={sources.map(source => ({value: source.id, label: source.name}))}
                        onChange={opt => onFilterChange('source_id', opt ? opt.value : undefined)}
                        className="m-1"
                        classNamePrefix="react-select"
                    />
                </Col>
            </Row>
        </Form>
    );
}

export default ArticleFilters;