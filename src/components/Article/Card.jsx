import React from 'react';

const ArticleCard = ({ id, title, author, date, description, content, url, imageUrl, color }) => (
    <article className={`postcard dark ${color}`}>
        <a className="postcard__img_link" href={content ? `/detail/${id}` : url}>
            <img className="postcard__img" src={imageUrl ? imageUrl : 'https://artsmidnorthcoast.com/wp-content/uploads/2014/05/no-image-available-icon-6.png'} alt="article"/>
        </a>
        <div className="postcard__text">
            <h1 className={`postcard__title ${color}`}><a href={content ? `/detail/${id}` : url}>{title}</a></h1>
            <div className="postcard__subtitle small">
                <time dateTime="2020-05-25 12:00:00">
                    <i className="fas fa-calendar-alt mr-2"></i>{date}
                </time>
            </div>
            <div className="postcard__bar"></div>
            <div className="postcard__preview-txt" dangerouslySetInnerHTML={{__html: description}} />
            {author && <ul className="postcard__tagbox">
                <li className={`tag__item play ${color}`}>
                    <i className="fas fa-play mr-2"></i>{author}
                </li>
            </ul>}
        </div>
    </article>
);

export default ArticleCard;