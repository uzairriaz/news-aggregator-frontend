import {useEffect, useState, useCallback} from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import Paginator from '../common/Paginator';
import Articles from '../../api/Articles';
import ArticleCard from './Card';
import ArticleFilters from './Filters';
import axios from 'axios';

let source;

const colors = ['blue', 'red', 'green', 'yellow'];

const ArticleList = () => {
    const [articles, setArticles] = useState({data: []});
    const [filters, setFilters] = useState({});
    const navigate = useNavigate();
    const params = useParams();
    const page = Number(params.page) || 1;

    const fetchArticles = useCallback((page = 1) => {
        if (source) {
            source.cancel();
        }
        source = axios.CancelToken.source();

        Articles.list({...filters, page}, source.token)
            .then(({data}) => setArticles(data))
            .catch((error) => error.code !== 'ERR_CANCELED' && console.error(error));
    }, [filters, setArticles]);

    useEffect(() => fetchArticles(page), [fetchArticles, page]);

    const onFilterChange = useCallback((key, value) => {
        setFilters({...filters, [key]: value});
        navigate(`/1`, {replace: true});
    },[filters, setFilters, navigate]);

    const onPageChange = useCallback(page => navigate(`/${page}`, {replace: true}), [navigate]);

    return (
        <section className="dark pt-2">
            <ArticleFilters onFilterChange={onFilterChange}/>
            <div className="container py-4">
                {articles.data.map((article, i) => (
                    <ArticleCard
                        key={article.id}
                        id={article.id}
                        title={article.title}
                        author={article.author.name}
                        date={new Date(article.published_at).toDateString()}
                        description={article.description}
                        content={article.content}
                        url={article.url}
                        imageUrl={article.image_url}
                        color={colors[(i + 1) % colors.length]}
                    />
                ))}
            </div>
            {articles.data.length > 0 && <div className="d-flex flex-wrap justify-content-center">
                <Paginator
                    page={page}
                    total={articles.total}
                    pageSize={articles.per_page}
                    onPageChange={onPageChange}
                />
            </div>}
        </section>
    );
};

export default ArticleList;