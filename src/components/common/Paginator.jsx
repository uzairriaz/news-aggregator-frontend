import {Pagination} from 'react-bootstrap';
import {useMemo} from "react";

const Paginator = ({page, onPageChange, total, pageSize}) => {
    const lastPage = total && pageSize ? Math.ceil(total / pageSize) : 0;

    const pages = useMemo(() => {
        let left = page - 2;
        let right = page;
        let pageCount = 0;
        const pages = [];

        while (left < page) {
            if (left > 0) {
                pages.push(left);
                pageCount++;
            }
            left++;
        }

        while (right <= lastPage && pageCount < 5) {
            pages.push(right++);
            pageCount++;
        }

        return pages;
    }, [page, lastPage]);

    return <Pagination className="pagination dark">
        <Pagination.First disabled={page === 1} onClick={() => onPageChange(1)}/>
        <Pagination.Prev disabled={page === 1} onClick={() => onPageChange(Math.max(page - 1, 1))}/>
        {pages.map(p => <Pagination.Item key={p} active={p === page}
                                         onClick={() => onPageChange(p)}>{p}</Pagination.Item>)}
        <Pagination.Next disabled={page === lastPage} onClick={() => onPageChange(Math.min(page + 1, lastPage))}/>
        <Pagination.Last disabled={page === lastPage} onClick={() => onPageChange(lastPage)}/>
    </Pagination>
};

export default Paginator;