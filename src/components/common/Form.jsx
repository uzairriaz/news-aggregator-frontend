import {useCallback, useState, Fragment} from 'react';
import {Button, Form as BootstrapForm} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const Form = ({fields, action, successCallback, submitText, submittingText}) => {
    const [values, setValues] = useState({});
    const [errors, setErrors] = useState({});
    const [isLoading, setLoading] = useState(false);

    const onChange = useCallback((event) => setValues({...values, [event.target.name]: event.target.value}), [values, setValues])

    const handleSubmit = useCallback((event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (form.checkValidity() === false) {
            event.stopPropagation();
            return;
        }

        setLoading(true);
        setErrors({});
        action(values)
            .then(res => successCallback(res))
            .catch(({response: {data}}) => setErrors(data))
            .then(() => setLoading(false));
    }, [values, action, setLoading, successCallback]);

    return <BootstrapForm onSubmit={handleSubmit}>
        {fields.map(field => (
            <Fragment key={field.name}>
                <div className="mb-3 input-group">
                    <span className="input-group-text dark text-white border-dark">
                        <FontAwesomeIcon icon={field.icon}/>
                    </span>
                    <input className="form-control" required name={field.name} type={field.type} placeholder={field.placeholder} onChange={onChange}/>
                    {errors[field.name] && <div className="text-danger w-100 mt-1 text-left">{errors[field.name][0]}</div>}
                </div>
            </Fragment>
        ))}
        {errors.message && <div className="text-danger w-100 my-1 text-left">{errors.message}</div>}

        <div className="d-grid">
            <Button variant="primary" type="submit" disabled={isLoading}>
                {isLoading ? submittingText : submitText}
            </Button>
        </div>
    </BootstrapForm>
}

export default Form;