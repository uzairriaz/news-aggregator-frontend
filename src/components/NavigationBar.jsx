import {useCallback} from 'react';
import { Navbar, Button } from 'react-bootstrap';
import Auth from '../api/Auth';

const NavigationBar = () => {
    const logout = useCallback(() => {
        Auth.logout()
            .catch(err => console.error(err))
            .then(() => {
                localStorage.removeItem('accessToken');
                window.location.href = '/login';
            });
    }, []);

    return <Navbar expand="lg" className="dark d-flex flex-row justify-content-between align-items-center">
        <Navbar.Brand href="/" className="d-flex align-items-center m-2 text-white">
            <strong>News Aggregator</strong>
        </Navbar.Brand>
        {localStorage.getItem('accessToken') && <div className="d-flex justify-content-end m-2">
            <a href="/preferences" className="mx-1 btn btn-outline-dark text-white">
                Preferences
            </a>
            <Button onClick={logout} variant="dark" className="mx-1">
                Logout
            </Button>
        </div>}
    </Navbar>
}

export default NavigationBar;