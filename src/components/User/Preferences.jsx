import {useCallback, useEffect, useMemo, useState} from 'react';
import {useNavigate} from 'react-router-dom';
import {Button, Card, Col, Container, Row} from 'react-bootstrap';
import Select from 'react-select';
import Categories from '../../api/Categories';
import Sources from '../../api/Sources';
import Authors from '../../api/Authors';
import Preferences from '../../api/Preferences';

const UserPreferences = () => {
    const navigate = useNavigate();
    const [authors, setAuthors] = useState([]);
    const [categories, setCategories] = useState([]);
    const [sources, setSources] = useState([]);
    const [preferences, setPreferences] = useState({authors: [], categories: [], sources: []});

    useEffect(() => {
        Preferences.get().then(({data}) => data && setPreferences(data));
        Authors.list().then(({data}) => setAuthors(data));
        Categories.list().then(({data}) => setCategories(data));
        Sources.list().then(({data}) => setSources(data));
    }, [setAuthors, setCategories, setSources, setPreferences]);

    const fields = useMemo(() => [
        {
            name: 'authors',
            placeholder: 'Authors',
            options: authors.map(author => ({value: author.id, label: author.name}))
        },
        {
            name: 'categories',
            placeholder: 'Categories',
            options: categories.map(category => ({value: category.id, label: category.name}))
        },
        {
            name: 'sources',
            placeholder: 'Sources',
            options: sources.map(source => ({value: source.id, label: source.name}))
        },
    ], [authors, categories, sources]);

    const onChange = useCallback((name, options) => setPreferences({...preferences, [name]: options.map(opt => ({id: opt.value, name: opt.label}))}), [preferences, setPreferences]);

    const savePreferences = useCallback(() => {
        const authors = preferences.authors.map(a => a.id);
        const categories = preferences.categories.map(c => c.id);
        const sources = preferences.sources.map(s => s.id);

        Preferences.store({authors, categories, sources}).then(() => navigate('/', {replace: true}))
    }, [preferences, navigate]);

    return <div className="preferences-form">
        <Container>
            <Row className="vh-100 d-flex justify-content-center align-items-center">
                <Col md={8} lg={6} xs={12} className="text-center">
                    <div className="border border-2 border-primary"></div>
                    <Card className="shadow px-4 dark">
                        <Card.Body>
                            <div className="mb-3 mt-md-4">
                                <div className="mb-3">
                                    <div className="d-flex flex-column">
                                        {fields.map(field => (
                                            <Select
                                                key={field.name}
                                                isMulti
                                                className="mb-2"
                                                name={field.name}
                                                value={preferences[field.name].map(p => ({value: p.id, label: p.name}))}
                                                options={field.options}
                                                placeholder={field.placeholder}
                                                onChange={options => onChange(field.name, options)}
                                                classNamePrefix="react-select"
                                            />
                                        ))}
                                    </div>
                                    <div className="d-grid">
                                        <Button variant="primary" onClick={savePreferences}>
                                            Save Preferences
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </div>
};

export default UserPreferences;