import './App.css';
import NavigationBar from './components/NavigationBar';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Register from './components/Auth/Register';
import Login from './components/Auth/Login';
import PageNotFound from './components/PageNotFound';
import ProtectedRoute from './components/ProtectedRoute';
import ArticleList from './components/Article/List';
import UserPreferences from './components/User/Preferences';
import ArticleDetail from './components/Article/Detail';

function App() {
  return (
      <>
          <NavigationBar />
          <BrowserRouter>
              <Routes>
                  <Route path="/:page?" element={<ProtectedRoute>
                      <ArticleList />
                  </ProtectedRoute>} />
                  <Route path="/detail/:id" element={<ProtectedRoute>
                      <ArticleDetail />
                  </ProtectedRoute>} />
                  <Route path="/preferences" element={<ProtectedRoute>
                      <UserPreferences />
                  </ProtectedRoute>} />
                  <Route path="/login" element={<Login />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="*" element={<PageNotFound />} />
              </Routes>
          </BrowserRouter>
      </>
  );
}

export default App;
