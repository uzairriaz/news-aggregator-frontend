import request from '../request';

const list = (params = {}) => {
    return request({
        url: '/sources',
        method: 'GET',
        params
    })
}

const Sources = {
    list
}

export default Sources;