import request from '../request';

const list = (params = {}) => {
    return request({
        url: '/authors',
        method: 'GET',
        params
    })
}

const Authors = {
    list
}

export default Authors;