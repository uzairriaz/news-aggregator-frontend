import request from '../request';

const list = (params = {}) => {
    return request({
        url: '/categories',
        method: 'GET',
        params
    })
}

const Categories = {
    list
}

export default Categories;