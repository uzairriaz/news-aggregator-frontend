import request from '../request';

const store = (data = {}) => {
    return request({
        url: '/preferences',
        method: 'POST',
        data
    })
}

const get = (params = {}) => {
    return request({
        url: '/preferences',
        method: 'GET',
        params
    })
}

const Preferences = {
    get,
    store,
}

export default Preferences;