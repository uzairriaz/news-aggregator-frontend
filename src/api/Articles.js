import request from '../request';

const list = (params = {}, cancelToken = null) => {
    return request({
        url: '/articles',
        method: 'GET',
        params,
        cancelToken,
    })
}

const get = (id) => {
    return request({
        url: `/articles/${id}`,
        method: 'GET',
    })
}

const Articles = {
    list,
    get,
}

export default Articles;