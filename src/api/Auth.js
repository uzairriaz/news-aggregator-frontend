import request from '../request';

const register = (data = {}) => {
    return request({
        url: '/auth/register',
        method: 'POST',
        data
    })
}

const login = (data = {}) => {
    return request({
        url: '/auth/login',
        method: 'POST',
        data
    })
}

const logout = () => {
    return request({
        url: '/auth/logout',
        method: 'POST',
    })
}

const Auth = {
    register,
    login,
    logout,
}

export default Auth;