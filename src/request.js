import axios from 'axios';

const request = (config = {}) => {
    return axios({
        ...config,
        headers: {Authorization: `Bearer ${localStorage.getItem('accessToken')}`},
        baseURL: process.env.REACT_APP_API_BASE_URL
    });
}

export default request;